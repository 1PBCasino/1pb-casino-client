const Byteball = require.main.require(path.join(__dirname, 'Byteball.js'));
const constants = require.main.require(path.join(__dirname, 'constants.js'));
const eventBus = Byteball.eventBus;
/**
 * @description Query your deposit address
 * @param {string} currency Currency you want to deposit
 * @example {method: 'deposit', result: 'OYW2XTDKSNKGSEZ27LMGNOPJSYIXHBHC'}
 * @returns {json}
 */
function deposit(currency) {
    let query = {method: currency==='bytes'?'deposit':'depositBTC', currency: currency};
    Byteball.query(query);
}
/**
 * @description Transfer your balance to your own wallet
 * @param {number} amount Amount to withdraw
 * @param {string} currency Currency to withdraw
 * @returns {void}
 */
function withdraw(amount, currency) {
    currency?currency=currency:currency='bytes';
    let query = {method: 'withdraw'};
    query.amount = parseInt(document.getElementById('wdraw').value);
    document.getElementById('wdraw').value ='';
    query.address = document.getElementById('addr').value;
    document.getElementById('addr').value ='';
    Byteball.query(query);
    setTimeout(function () {
        getBalance();
    }, 2000)
}
/**
 * @description Queries the balance on your 1PB Casino account
 * @example {method: 'getBalance', result: {bytes: 1234567890, blackbytes: 1234567890}}
 * @returns {json}
 */
function getBalance() {
    let query = {method: 'getBalance'};
    Byteball.query(query);
}
/**
 * @description Submit a bet
 * @param {number} type Type of bet: 1->half/2->double/other->normal
 * @param {number} amount Amount of Bytes at stake
 * @param {number} target Number (the roll must be higher/lower than target to win)
 * @param {number} currency bytes/blackbytes/BTC
 * @param {char} HL Higher/Lower, possible values are H and L ONLY
 * @example {method: 'bet', result: -1234567890}
 * @returns {json}
 */
function bet(type, amount, target, HL, currency) {
    let query = {method: 'bet', amount: amount, chance: target, HL: HL, currency: currency};
    if (type === 1)
        query.amount = parseInt(query.amount*0.5);
    else if (type === 2)
        query.amount *= 2;
    Byteball.query(query);
    setTimeout(function () {
        getBalance();
    }, 350)
}
/**
 * @description Invest your Bytes/BlackBytes
 * @param {number} amount Amount to invest
 * @param {string} currency Currency you want to invest (possible values are Bytes or BlackBytes)
 * @example {method: 'invest', result: OK}
 * @returns {json}
 */
function invest(amount, currency) {
    currency?currency=currency:currency='bytes';
    let query = {method:'invest', currency: currency, amount: amount};
    Byteball.query(query);
    setTimeout(function () {
        getBalance();
        getInvested();
    }, 1000)
}
/**
 * @description Divest your Bytes/BlackBytes
 * @param {number} amount Amount to invest
 * @param {string} currency Currency you want to invest (possible values are Bytes or BlackBytes)
 * @returns {void}
 */
function divest(amount, currency) {
    currency?currency=currency:currency='bytes';
    let query = {method: 'divest', amount: amount, currency: currency};
    Byteball.query(query);
}
/**
 * @description Broadcast msg to connected users
 * @param {string} msg Message to broadcast
 * @returns {void}
 */
function chat(msg) {
    let query = {method: 'chat', text: msg};
    Byteball.query(query);
}
/**
 * @description Used to pair the client with the server (call it only on first start)
 * @returns {void}
 */
function register() {
    let query = {method: 'register', pubkey: Byteball.PubKey};
    Byteball.query(query);
}
/**
 * @description Submits a bug report
 * @param {string} description description of the bug
 * @returns {void}
 */
function BUG_REPORT(description) {
    let query = {method: 'bug', description: description};
    Byteball.query(query);
}
/**
 * @description Used to populate the Account page
 * @example {method: 'populate', Invested: 1234567890, Wagered: 1234567890, NBets: 1234567890}
 * @returns {json}
 */
function populate() {
    let query = {method: 'populate'};
    Byteball.query(query);
}
/**
 * @description Get the latest 20 actions the user have performed
 * @example {method: 'populate', Invested: 1234567890, Wagered: 1234567890, NBets: 1234567890}
 * @returns {json}
 */
function getLastEvents() {
    let query = {method: 'getLastEvents'};
    Byteball.query(query);
}
/**
 * @description Send tip to user
 * @param amount {number} Amount of bytes to tip
 * @param user {string} DeviceID of the user who will receive the tip
 * @param currency {string} Currency you want to send (Bytes/BlackBytes)
 * @example {method: tipSND};
 * {method: tipRec}
 * @returns {json} tipSND to sender and tipREC to the receiver
 */
function tip(amount, user, currency) {
    currency?currency=currency:currency='bytes';
    let query = {method: 'tip', amount:amount, target:user, currency:currency};
    Byteball.query(query);
}
/**
 * @description Get global stats
 * @example {Bets: 1234567890, Wagered: 1234567890, Users: 1234567890}
 * @returns {json}
 */
function getTotals() {
    let query = {method: 'getTotals'};
    Byteball.query(query);
}
/**
 * @description Silent Smart Auto Updater, call it just once per execution. The server will send one update event for each file it sends to you.
 * It only sends files that have changed since the last version
 * @returns {array}
 */
function update() {
    let query = {method: 'update', version:JSON.parse(fs.readFileSync('./data/conf.json')).version};
    Byteball.query(query);
}
/**
 * @description Queries the amount of currencies invested
 * @example {method: 'getInvested', result: {bytes: 1234567890, blackbytes:1234567890}}
 * @returns {json}
 */
function getInvested() {
    let query = {method: 'getInvested'};
    Byteball.query(query);
}
/**
 * @description Queries the of tickets already punched for a given lotto
 * @param {number} amount The ticket cost of the punchCard
 * @example {method: 'getPunched', result: [0,1,2,3,4,5,6,7,8,9,0]}
 * @returns {json}
 */
function getPunched(amount) {
    let query = {method: 'getPunched', bet: amount};
    Byteball.query(query);
}
/**
 * @description Queries amount of Bytes in the casino bankroll
 * @example {method: 'getBankroll', result: 1234567890}
 * @returns {json}
 */
function getBankroll() {
    let query = {method: 'getBankroll'};
    Byteball.query(query);
}
/**
 * @description Punches the given ticket for the given lotto
 * @param {number} n ticket to punch
 * @param {number} bet cost per ticket (don't try to fool the server. It won't work)
 * @example {method: punch, result: false}
 * @returns {json} result is true if you win, false if you lost
 */
function punch(n, bet){
    let query = {method: 'punch', ticket: n, bet: parseInt(document.getElementById('bet').value)};
    Byteball.query(query);
    setTimeout(function () {
        getPunched();
        getBalance();
    }, 500)
}

/**
 * @description Queries the previous server seed (not the hash) for verification purposes
 * @example {"method":"getLastSeed","result":"57ff07f12f3c094696defe2f5966ef75bb4bfba9faac809f1c0f96cea03cebb0"}
 * @returns {json}
 */
function getLastSeed() {
    let query = {method:"getLastSeed"};
    Byteball.query(query);
}
/**
 * @description Queries amount hash of the current server seed
 * @example {method: 'getSHash', result: '17c385f6883e276ced989b2c95ecdd37e0532b5606244eeff0673f4feff6c005'}
 * @returns {json}
 */
function getSHash() {
    let query = {method: 'getSHash'};
    Byteball.query(query);
}
/**
 * @description Queries the unmatched RPS offers
 * @example {method: 'getRPSBook', result: [{from:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA, amount:1234567890}, {...}]}
 * @returns {json}
 */
function getRPSBook(){
    let query = {method: 'rpsBook'};
    Byteball.query(query);
}
/**
 * @description Join a given RPS game
 * @returns {void}
 */
function joinRPS(target, choice){
    let query = {method: 'joinRPS', user: target, choice: choice};
    Byteball.query(query);
}
/**
 * @description Invite a player to play RPS against you (they will get a notification). If no target is specified the game will be public
 * @param {string} target The friend you want to play against (DeviceID)
 * @param {number} amount Amount of bytes to put at stake
 * @param {enum} choice [R, P, S]
 * @param {enum} currency [bytes, blackbytes, BTC]
 * @returns {void}
 */
function inviteRPS(target, amount, choice, currency){
    let query = {method: 'newRPS', choice: choice, amount: amount, currency: ''};
    if (typeof target != undefined)
        query.partner = target;
    (typeof currency != undefined)  ?   query.currency = currency    :   query.currency = 'bytes';
    Byteball.query(query);
}
/**
 * @description Pretty-format large Byte amount
 * @example AppendPrefix(100000000000);
 * //100.00G
 * @param {number} n number to pretty format
 * @returns {string} pretty formated amount of bytes
 */
function AppendPrefix(n) {
        if (n>1000000000)
            return (n/1000000000).toFixed(2)+'G';
        if (n > 1000000)
            return (n/1000000).toFixed(2)+'M';
        if ((n > 1000))
            return (n/1000).toFixed(2)+'K';
        else
            n = n.toFixed(2);
        return n;
}
/**
 * @description Pull the lever of the slot machine
 * @example {method: 'roll', result: 1234567890}
 * @param {number} amount amount of bytes
 * @returns {number}
 */
function slots(amount) {
    let query = {method: 'slots', amount: amount};
    Byteball.query(query);
}
/**
 * @description Converts a pretty-printed amount of bytes to their correspondent byte amount
 * @example toBytes(100.00G);
 * //100000000000
 * @param {string} number pretty formatted number
 * @returns {number} amount of bytes
 */
function toBytes(number){
    let n;
    if (number.slice(-1) !== 'K' && number.slice(-1) !== 'M' && number.slice(-1)  !== 'G')
        n = number;
    else{
        n = number.slice(0, -1);
        if (number.slice(-1) === "K")
            n*=1000;
        else if (number.slice(-1) === "M")
            n*= 1000000;
        else if (number.slice(-1) === "G")
            n*= 1000000000;
    }
    return parseInt(n);
}

function getDeviceID(){
    return Byteball.getMyDeviceAddress();
}

exports.getDeviceID = getDeviceID;
exports.deposit = deposit;
exports.withdraw = withdraw;
exports.toBytes = toBytes;
exports.getLastSeed = getLastSeed;
exports.bet = bet;
exports.tip = tip;
exports.getBalance = getBalance;
exports.getBankroll = getBankroll;
exports.invest = invest;
exports.chat = chat;
exports.divest = divest;
exports.eventBus = eventBus;
exports.register = register;
exports.populate = populate;
exports.update = update;
exports.getInvested = getInvested;
exports.getPunched = getPunched;
exports.punch = punch;
exports.getSHash = getSHash;
exports.getLastEvents = getLastEvents;
exports.AppendPrefix = AppendPrefix;
exports.getRPSBook = getRPSBook;
exports.inviteRPS = inviteRPS;
exports.slots = slots
exports.joinRPS = joinRPS;