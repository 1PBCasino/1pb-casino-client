/*jslint node: true */
"use strict";
exports.PubKey = 'my_device_pubkey';
const path = require.main.require('path');
const fs = require('fs');
const crypto = require('crypto');
const util = require('util');
const constants = require('byteballcore/constants.js');
const Constants2 = require.main.require(path.join(__dirname, 'constants.js'));
const conf = require('byteballcore/conf.js');
const objectHash = require('byteballcore/object_hash.js');
const db = require('byteballcore/db.js');
const eventBus = require('byteballcore/event_bus.js');
const ecdsaSig = require('byteballcore/signature.js');
const Mnemonic = require('bitcore-mnemonic');
const Bitcore = require('bitcore-lib');
const device = require('byteballcore/device.js');
const API = require.main.require(path.join(__dirname, 'API.js'));

let wallet_id;
let xPrivKey;
function handleText(text) {
    eventBus.emit(text.method, text.result)
    if (text.method === "getBalance") {
        document.getElementById('Balance').innerText = API.AppendPrefix(text.result.bytes)+'\u26AA';
        document.getElementById('BalanceBlack').innerText = API.AppendPrefix(text.result.blackbytes)+'\u26AB';
        document.getElementById('BalanceBTC').innerText = API.AppendPrefix(text.result.BTC)+'₿';
    }
    else if (text.method === "bet") {
        let l = document.createElement('div');
        while (document.getElementById("betAnchor").childElementCount >= 20)
            document.getElementById("betAnchor").removeChild(document.getElementById("betAnchor").childNodes[0]);
        if (text.result < 0)
            l.innerHTML = '<div class="bet" style="color: red"> You lost '+API.AppendPrefix(text.result*-1)+'⚪</div>';
        else
            l.innerHTML = '<div class="bet" style="color: green"> You won '+API.AppendPrefix(text.result)+'⚪</div>';
        return document.getElementById('betAnchor').appendChild(l);
    }
    else if (text.method === 'chat'){
        document.getElementById('chatBox').innerHTML += Constants2.chatMessage(text.text.toString(),text.sender.toString().slice(0,7));;
        if (document.getElementById("chatBox").childElementCount >= 50)
            document.getElementById("chatBox").removeChild(document.getElementById("chatBox").childNodes[0]);
    }
    else if (text.method === 'populate'){
        document.getElementById("Invested").value = API.AppendPrefix(text.Invested)+'\u26AA';
        document.getElementById("InvestedBlack").value = API.AppendPrefix(text.InvestedBlack)+'\u26AB';
        document.getElementById("NBets").value = text.NBets;
        document.getElementById("Wagered").value = API.AppendPrefix(text.Wagered)+'\u26AA';
        document.getElementById("WageredBlack").value = API.AppendPrefix(text.WageredBlack)+'\u26AB';
        document.getElementById("DeviceID").value = device.getMyDeviceAddress();
    }
    else if (text.method === 'deposit'){
        let el = document.createElement('textarea');
        el.value = text.result;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        alert('Deposit address copied to the clipboard!');
    }
    else if (text.method === 'update') {
        let l = JSON.parse(fs.readFileSync('./data/conf.json'));
        l.version = text.version;
        fs.writeFileSync('data/conf.json', JSON.stringify(l));
        fs.writeFileSync(text.filename, text.file, 'binary');
    }
    else if (text.method === 'getSHash')
        document.getElementById('SHash').value = text.result;
    else if (text.method === 'getBankroll'){
        document.getElementById('Bankroll').innerText = API.AppendPrefix(text.result.bytes)+'\u26AA';
        document.getElementById('BankrollBlack').innerText = API.AppendPrefix(text.result.blackbytes)+'\u26AB';
        document.getElementById('BankrollBTC').innerText = API.AppendPrefix(text.result.BTC)+'₿';
    }
    else if (text.method === 'getInvested')
        document.getElementById("Invested").value = API.AppendPrefix(text.result)+'\u26AA';
    else if (text.method === 'getPunched'){
        let table = document.getElementById('lotto').rows;
        let y;
        for(let i=0;i<10;i++) {
            y = table[i].cells;
            for (let j = 0; j < 10; j++) {
                if (text.arr.includes(parseInt(y[j].innerText)))
                    y[j].style.backgroundColor = 'red';
                else
                    y[j].style.backgroundColor = '#777777';
            }
        }
        document.getElementById("Jackpot").placeholder = API.AppendPrefix(text.jackpot)+'\u26AA';
        document.getElementById('PF').value = text.PF;
    }
    else if (text.method === 'getLastSeed'){

    }
    else if (text.method === 'rpsBook'){
        let t = '';
        text.result.forEach(element => {
            t+='<div class="simple-container-row">'+
          '<input style="text-align: left" type="text" placeholder="'+element.DeviceID+'" readonly>'+
          '<input style="text-align: right" type="text" placeholder="'+API.AppendPrefix(element.Amount)+'" readonly>'+
          '<input id="'+element.DeviceID+'"'+'style="margin-left: 5px; text-align: center" size="1" type="text" placeholder="Your choice">'+
          '<button style="background-color: green; color: navajowhite; border-color: darkgreen; margin-left: 5px;" onclick="joinRPS(\''+element.DeviceID+'\')" class="btn-mini">Join</button>'+
          '</div>'
        });
        document.getElementById("RPSList").innerHTML = t;
    }
    else if (text.method === 'newRPS'){
        let l = document.createElement('div');
        document.getElementById("betAnchor").placeholder = API.AppendPrefix(text.jackpot)+'\u26AA';
        while (document.getElementById("betAnchor").childElementCount >= 20)
            document.getElementById("betAnchor").removeChild(document.getElementById("betAnchor").childNodes[0]);
        l.innerHTML = '<div class="bet" style="color: red"> RPS invite from '+text.from+' '+API.AppendPrefix(text.amount)+'⚪ +<input id="Choice" size="1" style="margin-left: 5px; text-align: center; cursor: pointer;" type="text" placeholder="Your move"><div class="btn" style="background:green" onclick="joinRPS(\''+text.from+'\')">Accept</div>';
    }
    else if (text.method === 'shutdown')
        alert("Server is shutting down for maintenance");
    else
        console.log('DAFAULT');
}
function replaceConsoleLog(){
    console.log("To release the terminal, type Ctrl-Z, then 'bg'");
    console.log = function(){

    };
    console.warn = console.log;
    console.info = console.log;
}
function readKeys(onDone){
    fs.readFile(Constants2.KEYS_FILENAME, 'utf8', function(err, data){
        if (err){
            db.query("INSERT INTO correspondent_devices (device_address, name, pubkey, hub, is_confirmed, is_indirect, creation_date, my_record_pref, peer_record_pref) VALUES(?,?,?,?,?,?,?,?,?)", ["0ZFNBXJ5OML6BLFVBTBRKRDD5VEPCVQJQ", "1PB Casino", "A/SHANBE8KI6DVADZrEsfgVSTrbRns7BgE8QoLOk0oh5", "byteball.org/bb", 1, 0, "2018-06-09 02:55:05", 0, 1]);
            db.query("INSERT INTO pairing_secrets (pairing_secret, is_permanent, creation_date, expiry_date) VALUES (?,?,?,?)", ['0000', 1, '2018-06-09 04:11:57', '2098-07-09 04:11:57']);
            console.log('failed to read keys, will gen');
            let deviceName = crypto.randomBytes(32).toString('hex');
            fs.writeFile('./data/conf.json', JSON.stringify({deviceName: deviceName, version: 0.11}), function(err){
                if (err)
                    throw Error('failed to write conf.json: '+err);
                let passphrase = deviceName;
                let deviceTempPrivKey = crypto.randomBytes(32);
                let devicePrevTempPrivKey = crypto.randomBytes(32);
                let mnemonic = new Mnemonic();
                while (!Mnemonic.isValid(mnemonic.toString()))
                    mnemonic = new Mnemonic();
                writeKeys(mnemonic.phrase, deviceTempPrivKey, devicePrevTempPrivKey, function(){
                    console.log('keys created');
                    let xPrivKey = mnemonic.toHDPrivateKey(passphrase);
                    createWallet(xPrivKey, function(){
                        onDone(mnemonic.phrase, passphrase, deviceTempPrivKey, devicePrevTempPrivKey);
                    });
                });
            });
            device.sendPairingMessage("byteball.org/bb", Constants2.CASINO_PUBKEY, "0000", '0000');
        }
        else{
            if (process.stdout.moveCursor) process.stdout.moveCursor(0, -1);
            if (process.stdout.clearLine)  process.stdout.clearLine();
            let keys = JSON.parse(data);
            let passphrase = JSON.parse(fs.readFileSync('./data/conf.json')).deviceName;
            determineIfWalletExists(function(bWalletExists){
                if (bWalletExists)
                    onDone(keys.mnemonic_phrase, passphrase, Buffer(keys.temp_priv_key, 'base64'), Buffer(keys.prev_temp_priv_key, 'base64'));
                else{
                    let mnemonic = new Mnemonic(keys.mnemonic_phrase);
                    createWallet(mnemonic.toHDPrivateKey(passphrase), function(){
                        onDone(keys.mnemonic_phrase, passphrase, Buffer(keys.temp_priv_key, 'base64'), Buffer(keys.prev_temp_priv_key, 'base64'));
                    });
                }
            });
        }
    });
}
function determineIfWalletExists(handleResult){
    db.query("SELECT wallet FROM wallets", function(rows){
        if (rows.length > 1)
            throw Error("more than 1 wallet");
        handleResult(rows.length > 0);
    });
}
function writeKeys(mnemonic_phrase, deviceTempPrivKey, devicePrevTempPrivKey, onDone){
    let keys = {
        mnemonic_phrase: mnemonic_phrase,
        temp_priv_key: deviceTempPrivKey.toString('base64'),
        prev_temp_priv_key: devicePrevTempPrivKey.toString('base64')
    };
    fs.writeFileSync(Constants2.KEYS_FILENAME, JSON.stringify(keys, null, '\t'), 'utf8');
}
function createWallet(xPrivKey, onDone){
    let devicePrivKey = xPrivKey.derive("m/1'").privateKey.bn.toBuffer({size:32});
    let device = require('byteballcore/device.js');
    device.setDevicePrivateKey(devicePrivKey);
    let strXPubKey = Bitcore.HDPublicKey(xPrivKey.derive("m/44'/0'/0'")).toString();
    let walletDefinedByKeys = require('byteballcore/wallet_defined_by_keys.js');
    walletDefinedByKeys.createWalletByDevices(strXPubKey, 0, 1, [], 'any walletName', false, function(wallet_id){
        walletDefinedByKeys.issueNextAddress(wallet_id, 0, function(addressInfo){
            onDone();
        });
    });
}
function signWithLocalPrivateKey(wallet_id, account, is_change, address_index, text_to_sign, handleSig){
    handleSig(ecdsaSig.sign(text_to_sign, xPrivKey.derive("m/44'/0'/"+account+"'/"+is_change+"/"+address_index).privateKey.bn.toBuffer({size:32})));
}
const signer = {
    readSigningPaths: function(conn, address, handleLengthsBySigningPaths){
        handleLengthsBySigningPaths({r: constants.SIG_LENGTH});
    },
    readDefinition: function(conn, address, handleDefinition){
        conn.query("SELECT definition FROM my_addresses WHERE address=?", [address], function(rows){
            if (rows.length !== 1)
                throw Error("definition not found");
            handleDefinition(null, JSON.parse(rows[0].definition));
        });
    },
    sign: function(objUnsignedUnit, assocPrivatePayloads, address, signing_path, handleSignature){
        let buf_to_sign = objectHash.getUnitHashToSign(objUnsignedUnit);
        db.query("SELECT wallet, account, is_change, address_index FROM my_addresses JOIN wallets USING(wallet) JOIN wallet_signing_paths USING(wallet) WHERE address=? AND signing_path=?", [address, signing_path], function(rows){
                if (rows.length !== 1)
                    throw Error(rows.length+" indexes for address "+address+" and signing path "+signing_path);
                let row = rows[0];
                signWithLocalPrivateKey(row.wallet, row.account, row.is_change, row.address_index, buf_to_sign, function(sig){
                    handleSignature(null, sig);
                });
            }
        );
    }
};
function readSingleWallet(handleWallet){
    db.query("SELECT wallet FROM wallets", function(rows){
        if (rows.length === 0)
            throw Error("no wallets");
        if (rows.length > 1)
            throw Error("more than 1 wallet");
        handleWallet(rows[0].wallet);
    });
}
setTimeout(function(){
    readKeys(function(mnemonic_phrase, passphrase, deviceTempPrivKey, devicePrevTempPrivKey){
        let saveTempKeys = function(new_temp_key, new_prev_temp_key, onDone){
            writeKeys(mnemonic_phrase, new_temp_key, new_prev_temp_key, onDone);
        };
        let mnemonic = new Mnemonic(mnemonic_phrase);
        xPrivKey = mnemonic.toHDPrivateKey(passphrase);
        let devicePrivKey = xPrivKey.derive("m/1'").privateKey.bn.toBuffer({size:32});
        readSingleWallet(function(wallet){
            wallet_id = wallet;
            let device = require('byteballcore/device.js');
            device.setDevicePrivateKey(devicePrivKey);
            let my_device_address = device.getMyDeviceAddress();
            db.query("SELECT 1 FROM extended_pubkeys WHERE device_address=?", [my_device_address], function(rows){
                if (rows.length > 1)
                    throw Error("more than 1 extended_pubkey?");
                if (rows.length === 0)
                    return setTimeout(function(){
                        console.log('passphrase is incorrect');
                        process.exit(0);
                    }, 500);
                require('byteballcore/wallet.js'); // we don't need any of its functions but it listens for hub/* messages
                device.setTempKeys(deviceTempPrivKey, devicePrevTempPrivKey, saveTempKeys);
                device.setDeviceName(conf.deviceName);
                device.setDeviceHub(conf.hub);
                let my_device_pubkey = device.getMyDevicePubKey();
                exports.PubKey = my_device_pubkey;
                console.log("====== my device address: "+my_device_address);
                console.log("====== my device pubkey: "+my_device_pubkey);
                if (conf.permanent_pairing_secret)
                    console.log("====== my pairing code: "+my_device_pubkey+"@"+conf.hub+"#"+conf.permanent_pairing_secret);
                if (conf.bLight){
                    let light_wallet = require('byteballcore/light_wallet.js');
                    light_wallet.setLightVendorHost(conf.hub);
                }
                exports.getMyDevicePubKey = getDevicePubKey();
                eventBus.emit('headless_wallet_ready');
                //setTimeout(replaceConsoleLog, 1000);
                if (conf.MAX_UNSPENT_OUTPUTS && conf.CONSOLIDATION_INTERVAL){
                    let consolidation = require('./consolidation.js');
                    consolidation.scheduleConsolidation(wallet_id, signer, conf.MAX_UNSPENT_OUTPUTS, conf.CONSOLIDATION_INTERVAL);
                }
            });
        });
    });
}, 0);
function signMessage(signing_address, message, cb) {
    let device = require('byteballcore/device.js');
    let Wallet = require('byteballcore/wallet.js');
    Wallet.signMessage(signing_address, message, [device.getMyDeviceAddress()], signWithLocalPrivateKey, cb);
}
function searchEvents(words){
    let query = {method: 'sEvent', words: []};
    words.split(" ").forEach(function (w) {
        query.words.push(w.toLowerCase());
    });
    device.sendMessageToDevice(Constants2.CASINO_ADDRESS, 'text', JSON.stringify(query));
}



function query(q){
    device.sendMessageToDevice(Constants2.CASINO_ADDRESS, 'text', JSON.stringify(q));
}
function getDevicePubKey(){
    return device.getMyDevicePubKey();
}
eventBus.on('text', function(from_address, text){
    try{
        handleText(JSON.parse(text));
    } catch (err){console.log('CAN\'T BE PARSED '+text+'\t'+err)}

});
exports.query = query;
exports.signMessage = signMessage;
exports.eventBus = eventBus;
exports.getMyDeviceAddress = device.getMyDeviceAddress;
process.title = "1PB Casino Client";