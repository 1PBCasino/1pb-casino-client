How to build from source:
1) Make sure you have node.js and npm installed
2) cd to this directory on a console
3) Execute npm install (it will take a lot of time)
4) Move byteballcore folder to node_modules (I just edited device.js to make it accept messages from unpaired devices and sqlite_pool.js to change the path where the database is created)
4) cd to node_modules/.bin
5) execute electron-rebuild -m ../.. -w keytar -p -f (will take a while and there is no progress indicator)
6) (optional) if there is no electron-rebuild executable you have to cd to the root of the source code and run npm install electron-rebuild
7) (optional) run step 5 if you needed to install electro-rebuild
8) install electron-packager with npm install -g electron-packager
9) run electron-packager . 1PBCasino -w --overwrite from the root of the source code (it will take a while).
You should change -w (windows) to your own OS and add an arch argument for archs distinct from x64, for example --platform=darwin --arch=x64//
will compile a MacOS executable for a x64 computer. --platform=linux will compile a linux (x64) executable
You can also use custom arguments (--asar has not been tested and it might or might not work)

10) Copy the output folder somewhere and delete the source code (it is included in your packaged app)

Execute the 1PB Casino executable 3 times. The first it will create the ./data directory. The second it will save the info needed to contact//
the server in the db and the third will start normally.


Explanation of what each file does:
API.js the API to communicate with the server through byteball websocket protocool.
Byteball.js a simple byteball light wallet that is needed to communicate with the server using the websocket protocool.//
It also handles the incoming response from the server (this will be refactored in the future to make it just emit//
the response through and event emiter and handle it on another file)
conf.js configuration
contants.js utility
main.js standard electron app
script.js the script that puts everything together (and the only one that is included in the index.html file)
slots.js the script used to generate and animate the canvas of the slots machine game

The casino opens a remote debugging connection on localhost:8315. You can open it using Google Chrome to debug errors or just watch what the//
app is doing (it contains a lot of debug info from byteballcore)