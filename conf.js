/*jslint node: true */
"use strict";
exports.port = null;
exports.bServeAsHub = false;
exports.bLight = true;
exports.bIgnoreUnpairRequests = false;
exports.storage = 'sqlite';
exports.hub = 'byteball.org/bb';