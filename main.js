const {app, BrowserWindow} = require('electron');
app.disableHardwareAcceleration();                      //Fixes blurry rendering
app.on('ready', function() {
    let mainWindow = new BrowserWindow({width: 900, height: 700, minWidth: 900, minHeight: 700, frame:true, title:'1PB Casino', zoomToPageWidth: true, show: false, icon: __dirname+'/assets/icons/png/64x64.png'});
    let loading = new BrowserWindow({width: 400, height: 500, frame: false, zoomToPageWidth: true, show: true, icon: __dirname+'/assets/icons/png/64x64.png', skipTaskbar: true});
    loading.loadURL('file://'+__dirname+'/loading.html');
    mainWindow.loadURL('file://'+__dirname+'/index.html');
    mainWindow.once('ready-to-show', function(){
      mainWindow.show();
      loading.hide();
      loading.close();
    });
  mainWindow.on('closed', function() {
    mainWindow = null;
  });
  loading.on('closed', function() {
      loading = null;
  });
});

app.on('window-all-closed', function() {
    if (process.platform !== 'darwin')
        app.quit();
});
/*
DEBUGGING
*/
app.commandLine.appendSwitch('remote-debugging-port', '8315');
app.commandLine.appendSwitch('host-rules', 'MAP * ');
