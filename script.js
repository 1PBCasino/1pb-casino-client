const path = require.main.require('path');
const API = require.main.require(path.join(__dirname, 'API.js'));
const constants = require.main.require(path.join(__dirname, 'constants.js'));
const crypto = require.main.require('crypto');
let exports = {};
const fs= require.main.require('fs');
const eventBus = API.eventBus;
let currency = 'bytes';
let text = '⚪⚪⚪⚪⚪';  // The message displayed
let chars = '⚫⚪₿$€❌⧫';  // All possible Charactrers
const scale = 48;  // Font size and overall scale
const breaks = 0.003;  // Speed loss per frame
const endSpeed = 0.05;  // Speed at which the letter stops
const firstLetter = 220;  // Number of frames untill the first letter stopps (60 frames per second)
const delay = 40;  // Number of frames between letters stopping
let charMap = [];
let offsetV = [];
let offset = [];
let spinning = false;
let canvas;
let ctx;
text = text.split('');
chars = chars.split('');
function randomsort(a, b) { 
	return Math.random()>.5 ? -1 : 1;
}
function reset(){
    for(let i=0;i<chars.length;i++){
      charMap[chars[i]] = i;
    }
    for(let i=0;i<text.length;i++){
      let f = firstLetter+delay*i;
      offsetV[i] = endSpeed+breaks*f;
      offset[i] = -(1+f)*(breaks*f+2*endSpeed)/2;
    }
    requestAnimationFrame(loop = function(){
      ctx.setTransform(1,0,0,1,0,0);
      ctx.clearRect(0,0,canvas.width,canvas.height);
      ctx.globalAlpha = 1;
      ctx.fillStyle = '#622';
      ctx.fillRect(0,(canvas.height-scale)/2,canvas.width,scale);
      for(let i=0;i<text.length;i++){
        ctx.fillStyle = '#ccc';
        ctx.textBaseline = 'middle';
        ctx.textAlign = 'center';
        ctx.setTransform(1,0,0,1,Math.floor((canvas.width-scale*(text.length-1))/2),Math.floor(canvas.height/2));
        let h = Math.ceil(canvas.height/2/scale)
        for(let j=-h;j<h;j++){
          let c = charMap[text[i]]+j-Math.floor(offset[i]);
          while(c<0)c+=chars.length;
            c %= chars.length;
          let s = 1-Math.abs(j)/(canvas.height/2/scale+1)
          ctx.globalAlpha = s
          ctx.font = scale*s + 'px Helvetica'
          ctx.fillText(chars[c],scale*i,(j)*scale);
        }
      }
    });
    spinning = false;
  }
  
  function roll(){
    requestAnimationFrame(loop = function(){
      ctx.setTransform(1,0,0,1,0,0);
      ctx.clearRect(0,0,canvas.width,canvas.height);
      ctx.globalAlpha = 1;
      ctx.fillStyle = '#622';
      ctx.fillRect(0,(canvas.height-scale)/2,canvas.width,scale);
      for(let i=0;i<text.length;i++){
        ctx.fillStyle = '#ccc';
        ctx.textBaseline = 'middle';
        ctx.textAlign = 'center';
        ctx.setTransform(1,0,0,1,Math.floor((canvas.width-scale*(text.length-1))/2),Math.floor(canvas.height/2));
        let o = offset[i];
        while(o<0)o++;
        o %= 1;
        let h = Math.ceil(canvas.height/2/scale)
        for(var j=-h;j<h;j++){
          let c = charMap[text[i]]+j-Math.floor(offset[i]);
          while(c<0)c+=chars.length;
          c %= chars.length;
          let s = 1-Math.abs(j+o)/(canvas.height/2/scale+1)
          ctx.globalAlpha = s
          ctx.font = scale*s + 'px Helvetica'
          ctx.fillText(chars[c],scale*i,(j+o)*scale);
        }
        offset[i] += offsetV[i];
        offsetV[i] -= breaks;
        if(offsetV[i]<endSpeed){
          offset[i] = 0;
          offsetV[i] = 0;
        }
      }
      requestAnimationFrame(loop);
    });
    setTimeout(reset, 9000);
    setTimeout(API.getBalance, 8700);
  }
  
  eventBus.on('slots', function(data){
    data>0?text='⚪⚪⚪⚪⚪'.split(''):text=chars;
    text.sort(randomsort);
    while (text.length>5)
      text.pop();
    roll();
  });
function invest() {
    API.invest();
}
function show(element) {
    document.getElementById(element).style.display = 'block';
}
function hide(element) {
    document.getElementById(element).style.display = 'none';
}
function loadBR(){
    document.getElementById('view').innerHTML = fs.readFileSync(path.join(__dirname, 'lotto.html'));
    API.getPunched(parseInt(document.getElementById('bet').value));
}
function dropP2P(state){
    if (state){
        let l = document.createElement('div');
        l.innerHTML ='<div class="nav-group-item" onclick="load(\'rps.html\')">RPS</div>';
        return document.getElementById('p2p').appendChild(l);
    }
    while (document.getElementById('p2p').childElementCount > 1)
        document.getElementById('p2p').removeChild(document.getElementById('p2p').lastChild);
}
function dropDeposit(state){
    if (state){
        let l = document.createElement('div');
        l.innerHTML= '';
        l.innerHTML+='<div class="nav-group-item" onclick="deposit(\'bytes\')">Bytes</div>';
        l.innerHTML+='<div class="nav-group-item" onclick="deposit(\'bytes\')">BlackBytes</div>';
        l.innerHTML+='<div class="nav-group-item" onclick="deposit(\'BTC\')">BTC</div>';
        return document.getElementById('deposit').appendChild(l);
    }
    while (document.getElementById('deposit').childElementCount > 1)
        document.getElementById('deposit').removeChild(document.getElementById('deposit').lastChild);
}
function joinRPS(User){
    let choice = document.getElementById(User).value;
    API.joinRPS(User, choice);
    API.getBalance;
    API.getRPSBook();

}
function inviteRPS(){
    let target = document.getElementById('Friend').value;
    let curr = currency;
    let choice = document.getElementById('Choice').value;
    let amount = parseInt(API.toBytes(document.getElementById('Amount').value));
    API.inviteRPS(target, amount, choice, curr);
    API.getBalance();
}
function loadInviteRPS(){
    let l = document.createElement('div');
    l.innerHTML = fs.readFileSync(path.join(__dirname, 'inviteRPS.html'));
    document.getElementById('rpsAnchor').appendChild(l);
}
function loadJoinRPS(){
    let l = document.createElement('div');
    l.innerHTML = fs.readFileSync(path.join(__dirname, 'joinRPS.html'));
    API.getRPSBook();
    document.getElementById('rpsAnchor').appendChild(l); 
}
function closeJoinRPS(){
    while (document.getElementById('rpsAnchor').childElementCount > 0)
        document.getElementById('rpsAnchor').removeChild(document.getElementById('rpsAnchor').firstChild);
}
function loadSlots(){
    document.getElementById('view').innerHTML = fs.readFileSync(path.join(__dirname, 'slots.html'));
    canvas = document.querySelector('canvas');
    ctx = canvas.getContext('2d');
    (onresize = function(){
        canvas.width = canvas.clientWidth;
        canvas.height = canvas.clientHeight;
      })();
      document.getElementById('roll').addEventListener('click', function(event){
        if (!spinning){
          spinning = true;
          API.slots(API.toBytes(document.getElementById('amount').value));
        }
      });
      reset();
}
function updateBalance(currency){
    API.getBalance();
    if (currency === 'bytes'){
        document.getElementById('Balance').style.color = 'goldenrod';
        document.getElementById('BalanceBlack').style.color = 'palegreen';
		document.getElementById('BalanceBTC').style.color = 'palegreen';
    }
    else if (currency === 'blackbytes'){
        document.getElementById('BalanceBlack').style.color = 'goldenrod';
        document.getElementById('Balance').style.color = 'palegreen';
		document.getElementById('BalanceBTC').style.color = 'palegreen';
    }
	else if (currency === 'BTC'){
        document.getElementById('BalanceBTC').style.color = 'goldenrod';
        document.getElementById('Balance').style.color = 'palegreen';
		document.getElementById('BalanceBlack').style.color = 'palegreen';
    }
}
function switchRPS(){
    if (document.getElementById('Choice').value === 'R')
        document.getElementById('Choice').value = 'P';
    else if (document.getElementById('Choice').value === 'P')
        document.getElementById('Choice').value = 'S';
    else
    document.getElementById('Choice').value = 'R';
}
function bet() {
    let params = {amount: API.toBytes(document.getElementById('Amount').value),
        chance: parseFloat(document.getElementById('Target').value).toFixed(2),
        currency: currency,
        HL: (document.getElementById('HiLo').value === 'Higher')?'H':'L'
    };
    API.bet(0, params.amount, params.chance, params.HL, params.currency);
}
function double() {
    let params = {amount: API.toBytes(document.getElementById('Amount').value),
        chance: parseFloat(document.getElementById('Target').value).toFixed(2),
        HL: (document.getElementById('HiLo').value === 'Higher')?'H':'L'
    };
    API.bet(2, params.amount, params.chance, params.HL, params.currency);
    document.getElementById('Amount').value = API.AppendPrefix(API.toBytes(document.getElementById('Amount').value)*2);
}
function half() {
    let params = {amount: API.toBytes(document.getElementById('Amount').value),
        chance: parseFloat(document.getElementById('Target').value).toFixed(2),
        HL: (document.getElementById('HiLo').value === 'Higher')?'H':'L'
    };
    API.bet(1, params.amount, params.chance, params.HL, params.currency);
    document.getElementById('Amount').value = API.AppendPrefix(API.toBytes(document.getElementById('Amount').value)*0.5);
}
function chat() {
    let msg = document.getElementById('chatInput').value;
    if (msg.substring(0, 4) === '/tip')
        tip(msg.substring(5, 37), API.toBytes(msg.slice(38)))
    else
        API.chat(msg);
    document.getElementById('chatInput').value = '';
    document.getElementById('chatBox').innerHTML += constants.chatMessage(msg);
}
function tip(target, amount){

}
function deposit(currency) {
    if (currency == 'bytes' || currency === 'blackbyes')
        API.deposit('bytes');
    else if (currency === 'BTC')
        API.deposit('BTC');
}
function withdraw() {
    API.withdraw();
}
function AppendPrefix(n) {
    if (n > 1000000000)
        n = (n/1000000000).toFixed(2)+'G';
    else if (n > 1000)
        n = (n/1000).toFixed(2)+'K';
    else if (n > 1000000)
        n = (n/1000000).toFixed(2)+'M';
    else
        n = parseInt(n);
    return n;
}
function calculateReturn() {
    if (document.getElementById('HiLo').value === 'Higher')
        return document.getElementById('WinAmount').value = API.AppendPrefix((API.toBytes(document.getElementById('Amount').value)*100/(100-document.getElementById('Target').value)-API.toBytes(document.getElementById('Amount').value))*0.99)+(currency==='bytes'?'\u26AA':'\u26AB');
    document.getElementById('WinAmount').value = API.AppendPrefix((API.toBytes(document.getElementById('Amount').value)*100/(100-(100-document.getElementById('Target').value))-API.toBytes(document.getElementById('Amount').value))*0.99)+(currency==='bytes'?'\u26AA':'\u26AB');
}
function HiLo(){
    (document.getElementById('HiLo').value === 'Higher')?document.getElementById('HiLo').value = 'Lower': document.getElementById('HiLo').value = 'Higher';
    calculateReturn();
}
function getBankroll(){
    API.getBankroll();
}
setTimeout(function () {
    API.register();
    API.getBalance();
    //API.update();
    API.getBankroll();
    API.getSHash();
}, 1000);
function loadChat(){
    document.getElementById("chatInput").addEventListener("keyup", function (event) {
        if (event.keyCode === 13)
            document.getElementById("chatSend").click();
    });
}
function load(element){
    document.getElementById('view').innerHTML = fs.readFileSync(path.join(__dirname, element));
}
function loadAccount() {
    document.getElementById('view').innerHTML = fs.readFileSync(path.join(__dirname, 'Account.html'));
    API.getBalance();
    API.getInvested();
    API.populate();
    document.getElementById('IButton').addEventListener('click', function(){
        hide('Withdraw');
        show('Investment');
    });
    document.getElementById('WButton').addEventListener('click', function(){
        hide('Investment');
        show('Withdraw');
    });
    document.getElementById('Version').placeholder = JSON.parse(fs.readFileSync(path.join(__dirname, 'data/conf.json'))).version;
}
function loadDice() {
    document.getElementById('view').innerHTML = fs.readFileSync(path.join(__dirname, 'dice.html'));
    API.getSHash();
}
function loadLotto() {
    document.getElementById('body').innerHTML = fs.readFileSync(path.join(__dirname, 'lotto.html'));
    API.getPunched();
}
function punch(n) {
    API.punch(n);
}
function getPunched() {
    API.getPunched();
}
setTimeout(function(){
    API.getBalance();
    API.getBankroll();
}, 1000);
function verify(){
    let serverSeed = document.getElementById('Seed').value;
    let betN = document.getElementById('BetN').value;
    let deviceID = API.getDeviceID();
    device.getElementById('Result').value = parseFloat(crypto.createHmac('SHA256', serverSeed).update(deviceID + betN).digest().readUInt32BE() / Math.pow(2, 32) * 100).toFixed(2)
    return parseFloat(crypto.createHmac('SHA256', serverSeed).update(deviceID + betN).digest().readUInt32BE() / Math.pow(2, 32) * 100).toFixed(2);
}