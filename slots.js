/* -------------------------------------- */
/* ------------  Settings  -------------- */
/* -------------------------------------- */
const eventBus = require('byteballcore/event_bus.js');
let text = '⚪⚪⚪⚪⚪';  // The message displayed
let chars = '⚫⚪₿$€❌⧫';  // All possible Charactrers
const scale = 48;  // Font size and overall scale
const breaks = 0.003;  // Speed loss per frame
const endSpeed = 0.05;  // Speed at which the letter stops
const firstLetter = 220;  // Number of frames untill the first letter stopps (60 frames per second)
const delay = 40;  // Number of frames between letters stopping
let charMap = [];
let offsetV = [];
let offset = [];
let spinning = false;
function randomsort(a, b) { 
	return Math.random()>.5 ? -1 : 1;
}

let canvas = document.querySelector('canvas');
let ctx = canvas.getContext('2d');
text = text.split('');
chars = chars.split('');
(onresize = function(){
  canvas.width = canvas.clientWidth;
  canvas.height = canvas.clientHeight;
})();

function reset(){
  for(let i=0;i<chars.length;i++){
    charMap[chars[i]] = i;
  }
  for(let i=0;i<text.length;i++){
    let f = firstLetter+delay*i;
    offsetV[i] = endSpeed+breaks*f;
    offset[i] = -(1+f)*(breaks*f+2*endSpeed)/2;
  }
  requestAnimationFrame(loop = function(){
    ctx.setTransform(1,0,0,1,0,0);
    ctx.clearRect(0,0,canvas.width,canvas.height);
    ctx.globalAlpha = 1;
    ctx.fillStyle = '#622';
    ctx.fillRect(0,(canvas.height-scale)/2,canvas.width,scale);
    for(let i=0;i<text.length;i++){
      ctx.fillStyle = '#ccc';
      ctx.textBaseline = 'middle';
      ctx.textAlign = 'center';
      ctx.setTransform(1,0,0,1,Math.floor((canvas.width-scale*(text.length-1))/2),Math.floor(canvas.height/2));
      let h = Math.ceil(canvas.height/2/scale)
      for(let j=-h;j<h;j++){
        let c = charMap[text[i]]+j-Math.floor(offset[i]);
        while(c<0)c+=chars.length;
          c %= chars.length;
        let s = 1-Math.abs(j)/(canvas.height/2/scale+1)
        ctx.globalAlpha = s
        ctx.font = scale*s + 'px Helvetica'
        ctx.fillText(chars[c],scale*i,(j)*scale);
      }
    }
  });
  spinning = false;
}

function roll(){
  requestAnimationFrame(loop = function(){
    ctx.setTransform(1,0,0,1,0,0);
    ctx.clearRect(0,0,canvas.width,canvas.height);
    ctx.globalAlpha = 1;
    ctx.fillStyle = '#622';
    ctx.fillRect(0,(canvas.height-scale)/2,canvas.width,scale);
    for(let i=0;i<text.length;i++){
      ctx.fillStyle = '#ccc';
      ctx.textBaseline = 'middle';
      ctx.textAlign = 'center';
      ctx.setTransform(1,0,0,1,Math.floor((canvas.width-scale*(text.length-1))/2),Math.floor(canvas.height/2));
      let o = offset[i];
      while(o<0)o++;
      o %= 1;
      let h = Math.ceil(canvas.height/2/scale)
      for(var j=-h;j<h;j++){
        let c = charMap[text[i]]+j-Math.floor(offset[i]);
        while(c<0)c+=chars.length;
        c %= chars.length;
        let s = 1-Math.abs(j+o)/(canvas.height/2/scale+1)
        ctx.globalAlpha = s
        ctx.font = scale*s + 'px Helvetica'
        ctx.fillText(chars[c],scale*i,(j+o)*scale);
      }
      offset[i] += offsetV[i];
      offsetV[i] -= breaks;
      if(offsetV[i]<endSpeed){
        offset[i] = 0;
        offsetV[i] = 0;
      }
    }
    requestAnimationFrame(loop);
  });
  setTimeout(reset, 9000);
}

eventBus.on('slots', function(data){
  data.win === true?text='⚪⚪⚪⚪⚪':text=chars;
  text.sort(randomsort);
  while (text.length>5)
    text.pop();
  roll();
});
canvas.addEventListener('click', function(event){
  if (!spinning){
    spinning = true;
    eventBus.emit('slots', {win:false});
  }
});
reset();